## Stack Zabbix Server + Zabbix Agent + Zabbix Frontend + Mysql

Repositório dedicado a manter o arquivo [docker-compose.yaml](docker-compose.yaml), para implementação do Zabbix em ambientes que utilizam Docker.

Todos os detalhes do desenvolvimento podem ser encontrados em: [https://sgoncalves.tec.br/crie-agora-mesmo-seu-ambiente-de-monitoramento-com-zabbix-utilizando-docker/](https://sgoncalves.tec.br/crie-agora-mesmo-seu-ambiente-de-monitoramento-com-zabbix-utilizando-docker/)
